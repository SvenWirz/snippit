package de.sven_wirz.snipping;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class DtoFileWriter {

    private String fileUrl;
    private Map<String, String> attributes;

    public DtoFileWriter(String fileUrl) {
        this.fileUrl = fileUrl;
        this.attributes = new HashMap<>();
    }

    public void write(AttributeMap attributeMap, BiConsumer<Map<String, String>, Map.Entry<Method, String>> attributeConsumer) {
        String result = "";

        ParserRegistry parserRegistry = ParserRegistry.getInstance();
        for (Map.Entry<Method, String> entry : attributeMap.getAttributeMap().entrySet()) {
            attributeConsumer.accept(attributes, entry);

            TemplateParser parser = parserRegistry.getParser(entry.getKey().getReturnType());
            if (parser != null) {
                String parse = parser.parse(attributes);
                result = parse + "\n"+result;
            }
        }

        try {
            FileUtils.write(new File(fileUrl), result, Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
