package de.sven_wirz.snipping;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class ParserRegistry {

    private static ParserRegistry INSTANCE;

    private Map<Type, TemplateParser> parserMap;

    private ParserRegistry() {
        parserMap = new HashMap<>();
    }

    public static ParserRegistry getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ParserRegistry();
        }
        return INSTANCE;
    }

    public void addParser(Type type, TemplateParser parser) {
        parserMap.put(type, parser);
    }

    public TemplateParser getParser(Type type) {
        if(type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type ownerType = parameterizedType.getRawType();
            return parserMap.get(ownerType);
        } else {
            return parserMap.get(type);
        }
    }
}
