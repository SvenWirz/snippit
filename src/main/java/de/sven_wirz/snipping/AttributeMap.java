package de.sven_wirz.snipping;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class AttributeMap {

    private Map<Method, String> attributeMap;

    public AttributeMap(Class<?> aClass) {
        this.attributeMap = new HashMap<>();
        createMap(aClass);
    }

    private void createMap(Class<?> aClass) {
        Stream.of(aClass.getMethods())
                .filter(method -> method.getName().startsWith("get"))
                .filter(method -> !method.getName().equals("getClass"))
                .forEach(this::addToMap);
    }

    private void addToMap(Method method) {
        String attribute = StringUtils.uncapitalize(method.getName().substring(3));
        attributeMap.put(method, attribute);
    }

    public Map<Method, String> getAttributeMap() {
        return attributeMap;
    }
}
