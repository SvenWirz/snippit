package de.sven_wirz.snipping;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Objects;

public class TemplateParser {

    private String template;

    public TemplateParser(String templateUrl) {
        this.template = getTemplateBy(templateUrl);
    }

    private String getTemplateBy(String templateUrl) {
        try {
            return IOUtils.toString(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(templateUrl)), Charset.defaultCharset());
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to find template");
        }
    }

    public String parse(Map<String, String> attributes) {
        String result = template;
        for(Map.Entry<String, String> entry : attributes.entrySet()) {
            result = result.replaceAll("\\{\\{"+entry.getKey()+"}}", entry.getValue());
        }
        return result;
    }
}
