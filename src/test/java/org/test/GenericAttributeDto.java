package org.test;

import java.util.List;

public class GenericAttributeDto {
    private List<String> stringListe;
    private List<Integer> integerList;

    public List<String> getStringListe() {
        return stringListe;
    }

    public void setStringListe(List<String> stringListe) {
        this.stringListe = stringListe;
    }

    public List<Integer> getIntegerList() {
        return integerList;
    }

    public void setIntegerList(List<Integer> integerList) {
        this.integerList = integerList;
    }
}
