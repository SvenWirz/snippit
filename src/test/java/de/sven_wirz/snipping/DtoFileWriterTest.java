package de.sven_wirz.snipping;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.test.GenericAttributeDto;
import org.test.NormalTypesDTO;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

class DtoFileWriterTest {

    @Test
    public void useCase() {
        Class<?> aClass = NormalTypesDTO.class;


        AttributeMap attributeMap = new AttributeMap(aClass);
        ParserRegistry.getInstance().addParser(String.class, new TemplateParser("complexStringTemplate.xhtml"));
        ParserRegistry.getInstance().addParser(Integer.class, new TemplateParser("complexIntegerTemplate.xhtml"));
        ParserRegistry.getInstance().addParser(List.class, new TemplateParser("selectOneMenu.xhtml"));

        DtoFileWriter dtoFileWriter = new DtoFileWriter("target/normalTypes.xhtml");

        BiConsumer<Map<String, String>, Map.Entry<Method, String>> biConsumer = (attributes, entry) -> {
            attributes.put("context", StringUtils.uncapitalize(aClass.getSimpleName()));
            attributes.put("attribut", entry.getValue());
            attributes.put("returnType", entry.getKey().getReturnType().getSimpleName());
        };
        dtoFileWriter.write(attributeMap, biConsumer);
    }

    @Test
    public void generics() {
        Class<?> aClass = GenericAttributeDto.class;

        AttributeMap attributeMap = new AttributeMap(aClass);
        ParserRegistry.getInstance().addParser(String.class, new TemplateParser("complexStringTemplate.xhtml"));
        ParserRegistry.getInstance().addParser(Integer.class, new TemplateParser("complexIntegerTemplate.xhtml"));
        ParserRegistry.getInstance().addParser(List.class, new TemplateParser("selectOneMenu.xhtml"));
        DtoFileWriter dtoFileWriter = new DtoFileWriter("target/generic.xhtml");

        BiConsumer<Map<String, String>, Map.Entry<Method, String>> biConsumer = (attributes, entry) -> {
            attributes.put("context", StringUtils.uncapitalize(aClass.getSimpleName()));
            attributes.put("attribut", entry.getValue());
            attributes.put("returnType", entry.getKey().getReturnType().getSimpleName());
            if(hasGenericReturnType(entry.getKey())) {
                attributes.put("generic", genericNameOf(entry.getKey()));
            } else {
                attributes.remove("generic");
            }
        };

        dtoFileWriter.write(attributeMap, biConsumer);
    }

    private String genericNameOf(Method key) {
        ParameterizedType type = (ParameterizedType) key.getGenericReturnType();
        Class<?> genericClass = (Class) type.getActualTypeArguments()[0];
        return genericClass.getSimpleName();
    }

    private boolean hasGenericReturnType(Method key) {
        Type genericReturnType = key.getGenericReturnType();
        return genericReturnType instanceof ParameterizedType;
    }
}