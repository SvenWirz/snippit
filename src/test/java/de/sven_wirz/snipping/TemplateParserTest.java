package de.sven_wirz.snipping;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TemplateParserTest {

    TemplateParser testSubject;

    @Test
    public void parsesTemplate() {
        testSubject = new TemplateParser("stringTemplate.xhtml");
        Map<String,String> attributes = new HashMap<>();
        attributes.put("attribut", "stringValue");

        String result = testSubject.parse(attributes);

        assertEquals("Ich habe das Attribut stringValue geparsed!", result);
    }

    @Test
    public void parsesComplexTemplate() {
        String expected = "<f:inputText id=\"input-stringValue\"\n" +
                "             label=\"#{i18n['model.normalTypesDto.stringValue']}\"\n" +
                "             value=\"#{dto.stringValue}\"/>";


        Map<String,String> attributes = new HashMap<>();
        attributes.put("attribut", "stringValue");
        attributes.put("context", "normalTypesDto");

        testSubject = new TemplateParser("complexStringTemplate.xhtml");
        String result = testSubject.parse(attributes);

        assertEquals(expected.trim(), result.trim());
    }

}