package de.sven_wirz.snipping;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.test.NormalTypesDTO;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ParserTest {

    AttributeMap testSubject;

    @Test
    public void classMapContainsAttributesOfClass() {
        testSubject = new AttributeMap(NormalTypesDTO.class);

        Assertions.assertAll(
                () -> assertTrue(testSubject.getAttributeMap().containsValue("stringValue")),
                () -> assertTrue(testSubject.getAttributeMap().containsValue("longValue")),
                () -> assertTrue(testSubject.getAttributeMap().containsValue("booleanValue")),
                () -> assertTrue(testSubject.getAttributeMap().containsValue("integerValue")),
                () -> assertTrue(testSubject.getAttributeMap().containsValue("bigDecimalValue"))
        );
    }
}