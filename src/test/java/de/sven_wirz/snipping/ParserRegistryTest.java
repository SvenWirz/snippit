package de.sven_wirz.snipping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.test.NormalTypesDTO;

import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParserRegistryTest {

    ParserRegistry testSubject;

    @BeforeEach
    public void setup() {
        testSubject = ParserRegistry.getInstance();
    }

    @Test
    public void testTypeMapping() throws NoSuchMethodException {
        TemplateParser templateParser = new TemplateParser("stringTemplate.xhtml");
        testSubject.addParser(String.class, templateParser);

        Method method = NormalTypesDTO.class.getMethod("getStringValue");

        TemplateParser result = testSubject.getParser(method.getGenericReturnType());

        assertEquals(templateParser, result);
    }

}